package pranu;

import java.util.*;

public class Test
{
public static void main(String args[])
{
	Scanner sc = new Scanner(System.in);
try{
	Square sq=new Square();
	Triangle tri=new Triangle();
	Circle cir=new Circle();
	Rectangle rect=new Rectangle();
	System.out.println("Enter the side of SQUARE : ");
	double side=sc.nextDouble();
	System.out.println("Enter the sides of RECTANGLE : ");
	double l=sc.nextDouble();
	double b=sc.nextDouble();
	System.out.println("Enter the radius of CIRCLE : ");
	double r=sc.nextDouble();
	System.out.println("Enter the base and height of TRIANGLE : ");
	double ba=sc.nextDouble();
	double h=sc.nextDouble();
	
	sq.setSide(side);
	rect.setLength(l);
	rect.setBreadth(b);
	cir.setRadius(r);
	tri.setBase(ba);
	tri.setHeight(h);
	System.out.println("The area of Square is:"+sq.area());
	System.out.println("The area of rectangle is:"+rect.area());
	System.out.println("The area of triangle is:"+tri.area());
	System.out.println("The area of circle is:"+cir.area());
}
finally {
	sc.close();
}
}
}
